const randomNumber = Math.floor(Math.random() * 10) + 1;
let remainingGuesses = 3;

function checkGuess(guess) {
  if (guess === randomNumber) {
    displayMessage(`Oikein! Luku oli ${randomNumber}.`);
    disableButtons();
  } else {
    remainingGuesses--;
    if (remainingGuesses === 0) {
      displayMessage(`Arvausyritykset loppuivat. Oikea luku oli ${randomNumber}.`);
      disableButtons();
    } else {
      const hint = guess < randomNumber ? 'suurempi' : 'pienempi';
      displayMessage(`Väärin! Arvaa uudelleen. Oikea luku on ${hint}. Sinulla on jäljellä ${remainingGuesses} arvausta.`);
    }
  }
}

function displayMessage(message) {
  document.getElementById('message').innerText = message;
}

function disableButtons() {
  const buttons = document.querySelectorAll('#buttons-container button');
  buttons.forEach(button => button.disabled = true);
}
